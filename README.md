gitlab page https://gitlab.com/userAA/auth-gitlab-english-comments.git
gitlab comment auth-gitlab-english-comments

project auth 
technologies used on the frontend: фронтенде:
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    axios,
    bootstrap,
    react,
    react-bootstrap,
    react-dom,
    react-router-dom,
    react-scripts,
    universal-cookie,
    web-vitals;

technologies used on the backend:
    bcrypt,
    body-parser,
    cors,
    dotenv,
    express,
    jsonwebtoken,
    mongoose,
    nodemon;

Implementing registration and authorization of user.
